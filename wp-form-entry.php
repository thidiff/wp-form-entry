<?php
   /*
   Plugin Name: Thidiff WP form entry
   description: Custom WP form entry
   Version: 1.0.0
   Author: ThiDiff Technologies
   */

// Register Custom Post Type
function news_letter() {

	$labels = array(
		'name'                  => _x( 'News Letter', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'News Letter', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'News Letter', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'News Letter', 'text_domain' ),
		'description'           => __( 'News Letter Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => false,
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 8,
		'menu_icon'             => plugins_url('icon_16.png',__FILE__),
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'news_letter', $args );

}
add_action( 'init', 'news_letter', 0 );

?>